import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'


export default function Home() {
  return (
    <>
      <div className="mainBg">
        <h5 className="adjust-mt"></h5>
        <h1 className='text-center  color1'>Amiel E. Manzano</h1>
        <h3 className='text-center  color2'>Full-Stack Web Developer</h3>
        <div className='text-center'><a href="#aboutMe"><i className="fas fa-chevron-down arrowDown"></i></a></div>
      </div>
      <div className='bg1 pb-2'>
        <h1 className='text-center color3 mb-5' id="aboutMe">About Me</h1>
        <div className="container">
          <div className="row text-center">
            <div className="col-lg-4 col-md-12">
                <div className='glass shadow-lg px-4 pb-2 my-4'>
                  <h2 className='color1 pt-3'><i class="fas fa-graduation-cap color1"></i>Education</h2>
                  <hr ></hr>
                  <h5 className='color1'><b>Primary</b></h5>
                  <p className='color1'>Revival Fellowship Baptist School</p>
                  <h5 className='color1'><b>Elementary</b></h5>
                  <p className='color1'>Candoni Central Elementary School</p>
                  <h5 className='color1'><b>Secondary</b></h5>
                  <p className='color1'>Colegio San Agustin - Bacolod</p>
                  <h5 className='color1'><b>College</b></h5>
                  <p className='color1'>University Of St. La Salle -
                  Bachelor of Science Major in Computer Engineering</p>
                  <h5 className='color1'><b>Certificate Course</b></h5>
                  <p className='color1'>Zuitt Coding Bootcamp Philippines</p>
                </div>
            </div>
            <div className="col-lg-4 col-md-12">
                <div className='glass shadow-lg px-4 my-4 pb-2'>
                  <h2 className='color1 pt-3'><i class="fas fa-briefcase color1"></i> Experience</h2>
                  <hr></hr>
                  <h4 className='color1'><b>IT Support Intern</b></h4>
                  <h5 className='color1 mb-4'><b>Ubiquity Global Services</b></h5>
                  <p className='color1 text-justify'>Maintains the computer networks of all types of organisations, providing technical support and ensuring the whole company runs smoothly. </p>
                  
                  <h4 className='color1'><b>Software Engineer</b></h4>
                  <h5 className='color1 mb-4'><b>Seedtech Inc.</b></h5>
                  <p className='color1 text-justify'>Part of an offshore engineering team which is responsible for building and maintaining websites usually catering Japanese clients. </p>
                </div>
            </div>
            <div className="col-lg-4 col-md-12 my-4">
                <div className='glass shadow-lg px-3 pb-2'>
                  <h2 className='color1 pt-3'><i class="fas fa-laptop-code color1"></i>Skills</h2>
                  <hr></hr>
                  <h5 className='color1'><b>HTML</b></h5>
                  <h5 className='color1'><b>CSS</b></h5>
                  <h5 className='color1'><b>Javascript</b></h5>
                  <h5 className='color1'><b>NodeJs</b></h5>
                  <h5 className='color1'><b>ExpressJs</b></h5>
                  <h5 className='color1'><b>MongoDB</b></h5>
                  <h5 className='color1'><b>ReactJs</b></h5>
                  <h5 className='color1'><b>NextJs</b></h5>
                  <h5 className='color1'><b>C++</b></h5>
                  <h5 className='color1'><b>PHP</b></h5>
                  <h5 className='color1'><b>mySQL</b></h5>
                  <h5 className='color1'><b>Shopify</b></h5>

                </div>
            </div>
            {/* end of cards */}
            <div className='col-12 mt-5'>
              <h1 className='color3 text-center mt-5'>Projects</h1>
              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Aemzano Booking System</h2>
                <hr></hr>
                <h5 className='color1'><b>Technologies used:</b></h5>
                <p className='color1'>HTML, CSS, Javascript, NodeJs, ExpressJs, MongoDB Atlas, Gitlab, Heroku</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://aem.trainee.gitlab.io/capstone-2-front-end">Open</a>
                </div>
              </div>

              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Budget Tracking App</h2>
                <hr></hr>
                <h5 className='color1'><b>Technologies used:</b></h5>
                <p className='color1'> NextJS/ReactJS, NodeJS, ExpressJS, MongoDB, Heroku, Google Login, SMTP</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://vast-cove-95172.herokuapp.com/login">Open</a>
                </div>
              </div>

              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Design Project</h2>
                <hr></hr>
                <h5 className='color1'><b>Title:</b></h5>
                <p className='color1'> The Development of Crop Management and Inventory System for Mushroom Farmers - firmware/hardware developer</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://www.facebook.com/groups/256184368395441/permalink/520636595283549/">Open</a>
                </div>
              </div>

              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Photography Website</h2>
                <hr></hr>
                <h5 className='color1'><b>Technologies used:</b></h5>
                <p className='color1'> GitHub, HTML, and CSS(Cascading Style Sheet)</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://aemphotography.github.io/website/">Open</a>
                </div>
              </div>

              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Nexstore (e-commerce)</h2>
                <hr></hr>
                <h5 className='color1'><b>Technologies used:</b></h5>
                <p className='color1'> Gitlab, NextJS, NodeJS</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://nexstorewebapp-9egzeas1a-nexstore.vercel.app/">Open</a>
                </div>
              </div>

              <div className='glass2 px-3 pb-3 mt-5'>
                <h2 className='color1 pt-3 '>Brass Life (Shopify)</h2>
                <hr></hr>
                <h5 className='color1'><b>Technologies used:</b></h5>
                <p className='color1'> Liquid, HTML, CSS, Javascript</p>
                <div className='pt-3 pb-2'>
                  <a target="_blank" className='openBtn' href="https://sobo-brass.com/">Open</a>
                </div>
              </div>

            </div>
            {/* end of projects */}
          </div>
          <footer className='mt-5 pt-5 mb-0 pb-0'>
          <p className='footer bg1 text-center color3'>Amiel E. Manzano |   <a href='https://www.facebook.com/amiel.manzano.9' target='_blank'><i class="fab fa-facebook"></i>Facebook</a> - <a href='https://www.instagram.com/aem_koala/' target='_blank'><i class="fab fa-instagram"></i>Instagram</a> - <a href='https://www.linkedin.com/in/amiel-manzano-64333b192/' target='_blank'><i class="fab fa-linkedin"></i>Linkedin</a></p>
          </footer>
        </div>

      </div>
    </>
  )
}
