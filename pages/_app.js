import '../styles/globals.css'
import NavBar from '../components/Navbar'
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';


function MyApp({ Component, pageProps }) {

  return (
    <>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
